FROM httpd
LABEL description="Assignment 5 - Linux"
LABEL assignment="5"
WORKDIR /usr/local/apache2/htdocs/
COPY app /usr/local/apache2/htdocs/
