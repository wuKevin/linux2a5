"use strict";

!(function() { 

    document.addEventListener("DOMContentLoaded", function(e) {

        //VARIABLE SECTION

        const allImageTypes = ["natural", "enhanced", "aerosol", "cloud"];

        let EpicApplication = {};
        EpicApplication.imageArray = [];
        EpicApplication.imgLink = undefined;
        EpicApplication.dateCache = {};
        EpicApplication.imageCache = {};

        let getImgType;
        let getNewImgType;
        let getDate;
        let url;

        let year;
        let month;
        let day;

        let template = document.getElementById("image-menu-item");
        let imgMenu = document.getElementById("image-menu");
        let imgDisplay = document.getElementById("earth-image");

        let index = 0;

        //run for default
        generateOptions();
        imgTypeChange(document.getElementById("type").value);

        //SUBMIT FORM SECTION
        const form = document.querySelector("form");
        form.addEventListener("submit", (event) => {
            event.preventDefault();

            getImgType = document.getElementById("type").value;
            getDate = document.getElementById("date").value;
            url = getURL(getImgType, getDate);

            //seperate date
            [year, month, day] = getDate.split("-");


            //if image cache for type and for date is not undefined
            if(EpicApplication.imageCache[getImgType].get(getDate) !== undefined){
                //use image cache
                generateList(EpicApplication.imageCache[getImgType].get(getDate));
            }else{
                //fetch
                fetch(url, {})
                    .then(response => {
                        if (!response.ok) {
                            throw new Error("Not 2xx response", {cause: response});
                        }
                        return response.json();
                    })
                    
                    .then( obj => {
                        obj.forEach(element => {
                            EpicApplication.imageArray.push(element.image);
                            //set map key to date and value to array of obj
                            EpicApplication.imageCache[getImgType].set(getDate, obj);
                        })
                        
                        generateList(obj);
                        
                    })

                    .catch( err => {
                        generateError();
                    })
            }
            
            
        });
        

        //DROPDOWN SECTION
        const typeDropDown = document.getElementById("type");
        typeDropDown.addEventListener("change", function(event){
            event.preventDefault();

            getNewImgType = document.getElementById("type").value;
            
            //if date cache of image type is not null
            if(EpicApplication.dateCache[getNewImgType] !== null){
                //use date cache
                setMax(EpicApplication.dateCache[getNewImgType]);
            }else{
                //fetch
                imgTypeChange(getNewImgType);
            }

        })


        
        //FUNCTION SECTION

        //generate DOM <option> and keys with the allImageTypes array
        function generateOptions(){
            
            //get select
            let optSelector = document.getElementById("type");

            //clear it
            optSelector.textContent = "";

            //generate options and append to selector + generate keys
            allImageTypes.forEach((type) => {

                //options
                const option = document.createElement("option");
                option.value = type;
                option.textContent = type;
                optSelector.appendChild(option);

                //keys for dateCache
                EpicApplication.dateCache[type] = null;

                //keys for imageCache
                EpicApplication.imageCache[type] = new Map();
            })

        }

        //generate url for api
        function getURL(imgType, date){
            return `https://epic.gsfc.nasa.gov/api/${imgType}/date/${date}`;
        }

        //generate the list
        function generateList(obj){

            //if no photo than generate error
            if(obj.length === 0){
                generateError();
                return;
            }

            //clear
            imgMenu.textContent="";
            index = 0;

            obj.forEach(element => {

                //clone template
                const clonedLi = template.content.cloneNode(true);
                imgMenu.appendChild(clonedLi);

                //get last li element, set attribute to the index, change index
                const lastLi = document.querySelector("li:last-child");
                lastLi.setAttribute("data-image-list-index", index);
                ++index;

                //get last span, add date
                const spans = document.getElementsByClassName("image-list-title")
                const lastSpan = spans[spans.length-1];
                lastSpan.textContent = element.date;

                //on list element click display
                lastLi.addEventListener("click", function(e){
                    display(getImgType, year, month, day, element.image, element.date, element.caption);
                });

            });
        }

        //display no image found when error or no image found
        function generateError(){

            let errMsg = "No image found!"

            //clear
            imgMenu.textContent="";


            //create li and span
            let errLi = document.createElement("li");
            imgMenu.appendChild(errLi);
            errLi.textContent = errMsg;

        }

        //display image, date and title
        function display(type, year, month, day, image, date, title){

            //display image
            EpicApplication.imgLink = `https://epic.gsfc.nasa.gov/archive/${type}/${year}/${month}/${day}/jpg/${image}.jpg`
            imgDisplay.setAttribute("src", EpicApplication.imgLink);

            //display date
            let imgDateDisplay = document.getElementById("earth-image-date");
            imgDateDisplay.textContent = date;

            //display title
            let imgTitleDisplay = document.getElementById("earth-image-title");
            imgTitleDisplay.textContent = title;

        }

        function imgTypeChange(imgType){
            const url = `https://epic.gsfc.nasa.gov/api/${imgType}/all`

            fetch(url, {})
                .then(response => {
                    if (!response.ok) {
                        throw new Error("Not 2xx response", {cause: response});
                    }
                    return response.json();
                })
                
                .then( obj => {
                    
                    //map
                    let dates = obj.map(obj => obj.date);
                    let datesList = dates.map(date => new Date(date));
                    //sort
                    datesList.sort((a, b) => b-a);
                    //retrieve latest
                    let latestDate = datesList[0];
                    //convert
                    let formated = formatDate(latestDate);
                    //set the max date
                    setMax(formated);

                    //store date in dateCache
                    EpicApplication.dateCache[imgType] = formated;
                    
                })

                .catch( err => {

                })
        }

        //formate date to yyyy-mm-dd
        function formatDate(date){
            let dateISOFormat = date.toISOString();
            let formated = dateISOFormat.split("T")[0];
            return formated;
        }

        //set the max input date
        function setMax(date){
            let dateInput = document.getElementById("date")
            dateInput.setAttribute("max", date);
        }


    });

}());